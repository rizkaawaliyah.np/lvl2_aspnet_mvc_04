﻿using LVL2_ASPNet_MVC_04.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using System.Web.Services.Description;

namespace LVL2_ASPNet_MVC_04.Controllers
{
    public class HomeController : Controller
    {
        Db_CustomerEntities db = new Db_CustomerEntities();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetData(Tbl_User user)
        {
            user = db.Tbl_User.Where(x => x.Id_User == 2).SingleOrDefault();
            /*var chk = new check
            {
                subject = "Hello " + param1,
                descriptions = param2 + " Years Old"
            };
            return JsonConvert.SerializeObject(chk);*/
            return Json(user, JsonRequestBehavior.AllowGet);
        }

        public ActionResult About()
        {
            return View();  
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }

    internal class check
    {
        public string subject { get; set; }
        public string descriptions { get; set; }
    }
}